
''' 
@package    Lab_0x02


@file       task_user.py
@brief      User interface task for cooperative multitasking example.
@details    Implements a finite state machine for the user task to ask the user
            what information they want from the encoder. 
@author     Wyatt Conner, James lavelle
@date       October 20, 2021
'''

import utime, pyb
from micropython import const
#from task_encoder import encoder_task

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)


class task_user(): 
     ''' 
         @brief      User interface task for cooperative multitasking example.
         @details    Implements a finite state machine
     '''
    
     def __init__(self, flag, period, POS_share, delta_share):
        '''
            @brief              Constructs an LED task.
            @details            The LED task is implemented as a finite state
                                machine.
            @param flag         The flag of the task to zero encoder
            @param POS_share    A shares.Share object used to hold the Position
                                brightness.
            @param delta_share  A shares.Share object used to hole the delta.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP
        '''
        #  Share varibles between files in the folder
        #
        ## The name of the task
        self.flag = flag
        ## A shares.Share object representing Encoder period
        self.period = period
        ## A shares.Share object representing Encoder Position
        self.POS_share = POS_share
        ## A shares.Share object representing Encoder Delta
        self.delta_share = delta_share
        ## State of the finite state machine
        self.state = S0_INIT
        ## The number of runs of the state machine
        self.runs = 0
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
     def run(self):
            '''
                @brief Runs one iteration of the FSM
            '''
            ## current time the iteration starts on
            self.current_time = utime.ticks_us()
            
            if self.state == S0_INIT:
                print('Welcome, press \'z\' to zero encoder',
                      '\'p\' to print the encoder position.'
                      '\'d\' to print the detla of the encoder',
                      '\'g\'Collect enocder data for 30 seconds',
                      '\'s\' end data collection Permaturely',
                      '\'h\' print command instructions')
                
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
               if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    
                    if(char_in == 'z' or char_in == 'Z'):
                        ## Name of the task
                        self.flag.write(const(0))
                     
                    elif(char_in == 'p' or char_in == 'P'):
                        print(self.POS_share.read())
                    
                    elif(char_in == 'd' or char_in == 'D'):
                        print(self.delta_share.read())
                       
                    elif(char_in == 'g' or char_in == 'g'):
                        
                        while(self.data_time <= 30):
                            self.present_time = utime.ticks_us()
                            self.data_time = self.present_time - self.current_time
                            
                            #encoder_task.run
                            
                            #print(self.data_time, self.POS_share)
                            
                            if(char_in == 's' or char_in == 's'):
                                self.data_time = 31
                    
                    elif(char_in == 'h' or char_in == 'h'):
                        self.state = S0_INIT
                    